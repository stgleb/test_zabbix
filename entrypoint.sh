#!/usr/bin/env bash
update_config_var() {
  local config_path=$1
  local var_name=$2
  local var_value=$3

  if [ "$(grep -E "^$var_name=" $config_path)" ]; then
        sed -i -e "/^$var_name=/s/=.*/=$var_value/" "$config_path"
        echo "updated"
    else
        sed -i -e "/^[#;] $var_name=/s/.*/&\n$var_name=$var_value/" "$config_path"
        echo "added"
    fi
}

DB_EXISTS=$(psql -U ${DB_SERVER_ZBX_USER} -W ${DB_SERVER_ZBX_PASS} -h ${DB_SERVER_HOST} -p ${DB_SERVER_PORT} -d ${DB_SERVER_DBNAME} -c "SELECT 1 AS result FROM pg_database WHERE datname='${DB_SERVER_DBNAME}'")

if [-z ${DB_EXISTS}]: then
  zcat /usr/share/doc/zabbix-server-pgsql/create.sql.gz | psql -U ${DB_SERVER_ZBX_USER} -W ${DB_SERVER_ZBX_PASS } -h ${DB_SERVER_HOST} -p ${DB_SERVER_PORT} -d ${DB_SERVER_DBNAME}

ZBX_CONFIG="/etc/zabbix/zabbix_server.conf"
update_config_var $ZBX_CONFIG "DBHost" "${DB_SERVER_HOST}"
update_config_var $ZBX_CONFIG "DBName" "${DB_SERVER_DBNAME}"
update_config_var $ZBX_CONFIG "DBUser" "${DB_SERVER_ZBX_USER}"
update_config_var $ZBX_CONFIG "DBPort" "${DB_SERVER_PORT}"
update_config_var $ZBX_CONFIG "DBPassword" "${DB_SERVER_ZBX_PASS}"

/usr/sbin/zabbix_server --foreground -c /etc/zabbix/zabbix_server.conf
