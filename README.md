# How to use this image

## Start `test_zabbix`

Start a Zabbix server container as follows:

    docker run --name some-zabbix-server-pgsql -e DB_SERVER_HOST="some-postgres-server" -e DB_SERVER_DBNAME="some-db-name" -e DB_SERVER_ZBX_USER="some-user" -e DB_SERVER_ZBX_PASS="some-password" -d stgleb95/test_zabbix:latest

## Default env
| header | header |
| ------ | ------ |
| DB_SERVER_HOST | localhost |
| DB_SERVER_DBNAME | zabbix |
| DB_SERVER_ZBX_USER | zabbix |
| DB_SERVER_PORT=5432 | 5432 |
| DB_SERVER_ZBX_PASS | zabbix |
