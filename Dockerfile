FROM ubuntu:18.04

ENV DB_SERVER_HOST=localhost \
    DB_SERVER_DBNAME=zabbix \
    DB_SERVER_ZBX_USER=zabbix \
    DB_SERVER_PORT=5432 \
    DB_SERVER_ZBX_PASS=zabbix

RUn apt update && apt install -y --no-install-recommends --no-install-suggests curl \
            ca-certificates \
            gpg \
            dirmngr \
            gpg-agent \
            iputils-ping \
            traceroute \
            fping \
            libcurl4 \
            libevent-2.1 \
            libopenipmi0 \
            libpcre3 \
            libpq5 \
            libsnmp30 \
            libssh2-1 \
            libssl1.1 \
            libxml2 \
            postgresql-client \
            snmp-mibs-downloader \
            unixodbc

RUN  wget --no-check-certificate https://repo.zabbix.com/zabbix/4.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.0-2+bionic_all.deb && \
      dpkg -i zabbix-release_4.0-2+bionic_all.deb && \
      apt update && \
      rm -rf /etc/dpkg/dpkg.cfg.d/excludes

RUN apt -y --no-install-recommends install zabbix-server-pgsql php7.2-pgsql

USER root
COPY ["entrypoint.sh", "/usr/bin/"]
ENTRYPOINT ["/usr/bin/entrypoint.sh"]

EXPOSE 10051/TCP
WORKDIR /var/lib/zabbix
CMD ["/usr/sbin/zabbix_server", "--foreground", "-c", "/etc/zabbix/zabbix_server.conf"]
